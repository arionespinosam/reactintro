import { useState } from "react";
import styled from "styled-components";

import Card from './Card';
import Submenu from './Submenu';

const Container = styled.div`

`

function App() {

  const [data, setData] = useState([{
      id:1 ,
      color: "#21d0d0",
      heigth: true,
      options: [
        {title:"PRICE LOW TO HIGH", state: false},
        {title:"PRICE HIGH TO LOW", state: false},
        {title:"popularity", state: true}
      ],
      footer: 2
    },
    {
      id:2,
      color: "#ff7745",
      heigth: false,
      options: [
        {title:"1UP NUTRIRION", state: false},
        {title:"ASITIS", state: false},
        {title:"AVVATR", state: false},
        {title:"BIG MUSCLES", state: false},
        {title:"BPI SPORTS", state: false},
        {title:"BSN", state: false},
        {title:"cellucor", state: true},
        {title:"DOMIN8R", state: true},
        {title:"DYMATIZE", state: true},
      ],
      footer: 1
    },
  ])
  
  const handleChange = (value, id) => {

      const match = data.filter(el => el.id === id)[0]
      const act = match.options.filter(el => el.title === value)[0]

      const copy = [...match.options]
      //find index of item to be replace
      const targetIndex = copy.findIndex( f => f.title === value)

      if(targetIndex > -1){
          copy[targetIndex] = {title: value, state: !act.state}
          //setData(copy)
          match.options = copy
      }

      const copyList = [...data]
      const targetIndexList = copyList.findIndex(f => f.title === id)
      if(targetIndexList > -1){
        copy[targetIndexList] = match
      }

      setData(copyList)
  }

  const handleReset = (id) => {
    const copy = [...data]
    const targetIndex = copy.findIndex(f=>f.id === id)
    if(targetIndex > -1){
      var raw = []
      const options = data.filter(el => el.id === id)[0].options
      for(let x of options) raw.push({ title: x.title, state: false})
      copy[targetIndex].options = raw
    }
    setData(copy)
  }

  return (
    <>
        <Container data={data}>
          <Card />
          {
            data.map((v, i)=> <Submenu {...v} onChange={handleChange} onReset={handleReset} /> ) 
          }
        </Container>

    </>
  );
}

export default App;
