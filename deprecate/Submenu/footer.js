import styled from 'styled-components';
import { Circle, Icon } from './Options';

import check from '../img/check.png';
import cancel from '../img/cancel.png';
import reset from '../img/reset.png';

const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
`

const ContainerV1 = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    margin-top: 15px;
    padding-top: 25px;
    padding-bottom: 25px;
    border-top: 1px solid #ffffff80;
`

const Item = ({v, onClick}) => {
    return (
        <Circle visible={true} style={{ marginLeft:10, height:50, width:50}} onClick={onClick}>
            <Icon visible={true} src={v} style={{ height:50, width:50 }}/>
        </Circle>
    )
}


const App = ({ version, onReset, onCancel, onAccept }) => {

    const handleReset = () => {
        console.log('Vamos a resetear');
        if(typeof onReset === 'function') onReset()
    }
    const handleOk = () => {
        console.log('Vamos OK');
        if(typeof onReset === 'function') onAccept()
    }
    const handleCancel = () => {
        console.log('Vamos a Cancelar');
        if(typeof onReset === 'function') onCancel()
    }

    if(version == 1){
        let data = [
            {icon: cancel, onClick: handleCancel}, 
            {icon: reset, onClick: handleReset}, 
            {icon: check, onClick: handleOk}
        ]
        return ( 
            <ContainerV1>
                {data.map((v, i) => <Item v={v.icon} onClick={v.onClick} />)}
            </ContainerV1>
        )
    }
    if(version == 2){
        let data = [
            {icon: cancel, onClick: handleCancel}, 
            {icon: check, onClick: handleOk}
        ]
        return ( 
            <Container>
                {data.map((v, i) => <Item v={v.icon} onClick={v.onClick} />)}
            </Container>
        )
    }

    return <></>
    

    
}
 
export default App;