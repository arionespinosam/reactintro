import styled from 'styled-components';
import Options from './Options';
import Footer from './footer';
import { type } from '@testing-library/user-event/dist/type';

const Container = styled.div`
    margin-top: 15px;
    background-color: ${props => props.color};
    border-radius: 15px;
    height: ${props => props.heigth?'200px':'auto'};
    width: 400px;
    padding-top: 15px;
    margin-bottom: 45px;
`

const App = (params) => {

    const handleChange = (value) => {
        if (typeof params.onChange === 'function') params.onChange(value, params.id)
    }

    const handleReset = () => {
        if (typeof params.onReset === 'function') params.onReset(params.id)
    }
    
    return ( 
        <Container color={params.color} heigth={params.heigth}>
            { params.options.map((v, i) => <Options key={i} {...v} onChange={handleChange}/> )}

            <Footer version={params.footer} onReset={handleReset} />
        </Container>
     );
}
 
export default App;