import styled from 'styled-components';

import check from '../img/check.png';

const Container = styled.div`
    margin-left: 25px;
    margin-top: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-right: 25px;
    align-items: center;
`
const Title = styled.p`
    margin: 0px;
    color: white;
    font-weight: 700;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    text-transform: uppercase;

`
export const Circle = styled.div`
    border-radius: 50%;
    border: 2px solid white;
    height: 30px;
    width: 30px;
    cursor: pointer;
    background-color: ${ props => props.visible ? 'white':'transparent'};
    border: solid 0.1px #f0f1f2;
    box-shadow: 1px 1px 1px 1px rgba(0, 0, 0, 0.1);
    padding: 5px;
`
export const Icon = styled.img`
    height: 30px;
    width: 30px;
    object-fit: contain;
    border-radius: 50%;
    visibility: ${ props => props.visible ? 'visible':'hidden'};
`

const App = ({ title, state, onChange }) => {

    const handleChange = () => {
        if(typeof onChange === 'function') onChange(title)
    }

    return ( 
        <>
            <Container>
                <Title>
                    {title}
                </Title>
                <Circle visible={state} onClick={handleChange}>
                    <Icon visible={state} src={check} />
                </Circle>
            </Container>
        </>
     );
}
export default App;